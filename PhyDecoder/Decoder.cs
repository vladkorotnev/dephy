﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace PhyDecoder
{
    public class GoodReader
    {
        public readonly Stream innerStream;
        public GoodReader(Stream stream)
        {
            innerStream = stream;
        }
        public bool IsAtEnd { get { return (innerStream.Position == innerStream.Length); } }
        public int Read()
        {
            return innerStream.ReadByte(); 
        }
        public int Peek()
        {
            long prevPos = innerStream.Position;
            int temp = innerStream.ReadByte();
            innerStream.Position = prevPos;
            return temp;
        }
    }
    public class DecodedQuestion
    {
        public string Text = "";
        public List<string> Answers = new List<string>();
        public short CorrectAnswer = 0;
    }
    public class DecodedSection
    {
        public string Title = "";
        public List<DecodedQuestion> Questions = new List<DecodedQuestion>();
    }
    public class DecodedQuestionnaire
    {
        public string Title = "";
        public List<DecodedSection> Sections = new List<DecodedSection>();
    }
    public static class Decoder
    {
        public static void noop() { }
        public static DecodedQuestionnaire Decode(string FilePath, bool verbose = false)
        {
            DecodedQuestionnaire result = new DecodedQuestionnaire();
            if (verbose) Console.WriteLine("LOG: Scanning file " + FilePath);
            using (Stream src = File.OpenRead(FilePath))
            {
                GoodReader r = new GoodReader(src);
                int cb = r.Read();

                string qTitle = "";
                while (cb != 0xD3)
                {
                    qTitle += WeirdByteToChar(cb, verbose);
                    cb = r.Read();
                }
                result.Title = qTitle;
               if(verbose) Console.WriteLine("LOG: Found Title: " + qTitle);

               
                    cb = r.Read();
                    while (cb == 0xD3)
                    {
                        DecodedSection s = new DecodedSection();
                        /* D3
                         * < section title >
                         * D3 BD
                         */
                        cb = 0;
                        while (cb != 0xD3 && r.Peek() != 0xBD)
                        {
                            cb = r.Read();
                            if (cb != 0xD3)
                            {
                                s.Title += WeirdByteToChar(cb, verbose);
                            }
                        }
                        if(verbose) Console.WriteLine("     Found section: "+s.Title);

                       // r.Read(); // skip the BD

                        // we now stand in front of a B0
                        
                        /* B0
                         * <question text>
                         * [ends when count of B5 bytes is 5]
                         * <ans1>
                         * B5
                         * <ans2>
                         * B5
                         * <ans3>
                         * B5
                         * <ans4>
                         * B5
                         * <ans5>
                         * B5
                         * {92 or 91 or 90 or 97 or 96}
                         */

                        cb = r.Read();
                        while (cb == 0xB0)
                        {
                            DecodedQuestion q = new DecodedQuestion();

                            int b5count = 0;
                            while (b5count < 5)
                            {
                                cb = r.Read();
                                if (cb == 0xB5) b5count++;
                                
                                q.Text += WeirdByteToChar(cb,verbose);
                            }

                            if(verbose) Console.WriteLine("LOG:         Found Question: " + q.Text);

                            while (r.Peek() == 0xB5) cb = r.Read();

                            // now we are in front of the first answer
                            for (int i = 0; i < 5; i++)
                            {
                                string abuf = "";
                                while (r.Peek() != 0xB5)
                                {
                                    cb = r.Read();
                                    abuf += WeirdByteToChar(cb,verbose);
                                }
                               if(verbose) Console.WriteLine("LOG:             Found Answer:"+abuf);
                                q.Answers.Add(abuf);
                                while (r.Peek() == 0xB5) cb = r.Read();
                            }
                            while (r.Peek() == 0xB5) cb = r.Read();
                            // get the correct ans
                            cb = r.Read();
                            switch (cb)
                            {
                                case 0x92:
                                    q.CorrectAnswer = 0;
                                    break;

                                case 0x91:
                                    q.CorrectAnswer = 1;
                                    break;

                                case 0x90:
                                    q.CorrectAnswer = 2;
                                    break;

                                case 0x97:
                                    q.CorrectAnswer = 3;
                                    break;
                                case 0x96:
                                    q.CorrectAnswer = 4;
                                    break;

                                default:
                                    q.CorrectAnswer = -1;
                                    break;
                            }
                            if (verbose) Console.WriteLine("LOG:             Correct Answer:" + q.CorrectAnswer.ToString());
                            s.Questions.Add(q);
                            cb = r.Read();
                            noop();
                        }
                        

                        result.Sections.Add(s);
                    }
                

            }
            return result;
        }

        static Dictionary<int, char> WeirdCodingTable = new Dictionary<int, char>()
        {
            {0x33, 'а'}, {0x32, 'б'}, {0x31, 'в'}, {0x30, 'г'}, {0x37, 'д'}, {0x36, 'е'}, {0x35, 'ж'}, {0x34, 'з'}, {0x3B, 'и'}, {0x3A, 'й'}, {0x39, 'к'}, {0x38, 'л'}, {0x3F, 'м'}, {0x3E, 'н'}, {0x3D, 'о'}, {0x3C, 'п'}, {0x73, 'р'}, {0x72, 'с'}, {0x71, 'т'}, {0x70, 'у'}, {0x77, 'ф'}, {0x76, 'х'}, {0x75, 'ц'}, {0x74, 'ч'}, {0x7B, 'ш'}, {0x7A, 'щ'}, {0x79, 'ъ'}, {0x78, 'ы'}, {0x7F, 'ь'}, {0x7E, 'э'}, {0x7D, 'ю'}, {0x7C, 'я'},
            {0x13, 'А'}, {0x12, 'Б'}, {0x11, 'В'}, {0x10, 'Г'}, {0x17, 'Д'}, {0x16, 'Е'}, {0x15, 'Ж'}, {0x14, 'З'}, {0x1B, 'И'}, {0x1A, 'Й'}, {0x19, 'К'}, {0x18, 'Л'}, {0x1F, 'М'}, {0x1E, 'Н'}, {0x1D, 'О'}, {0x1C, 'П'}, {0x03, 'Р'}, {0x02, 'С'}, {0x01, 'Т'}, {0x00, 'У'}, {0x07, 'Ф'}, {0x06, 'Х'}, {0x05, 'Ц'}, {0x04, 'Ч'}, {0x0B, 'Ш'}, {0x0A, 'Щ'}, {0x09, 'Ъ'}, {0x08, 'Ы'}, {0x0F, 'Ь'}, {0x0E, 'Э'}, {0x0D, 'Ю'}, {0x0C, 'Я'},
            {0xD2, 'A'}, {0xD1, 'B'}, {0xD0, 'C'}, {0xD7, 'D'}, {0xD6, 'E'}, {0xD5, 'F'}, {0xD4, 'G'}, {0xDB, 'H'}, {0xDA, 'I'}, {0xD9, 'J'}, {0xD8, 'K'}, {0xDF, 'L'}, {0xDE, 'M'}, {0xDD, 'N'}, {0xDC, 'O'}, {0xC3, 'P'}, {0xC2, 'Q'}, {0xC1, 'R'}, {0xC0, 'S'}, {0xC7, 'T'}, {0xC6, 'U'}, {0xC5, 'V'}, {0xC4, 'W'}, {0xCB, 'X'}, {0xCA, 'Y'}, {0xC9, 'Z'},
            {0xF2, 'a'}, {0xF1, 'b'}, {0xF0, 'c'}, {0xF7, 'd'}, {0xF6, 'e'}, {0xF5, 'f'}, {0xF4, 'g'}, {0xFB, 'h'}, {0xFA, 'i'}, {0xF9, 'j'}, {0xF8, 'k'}, {0xFF, 'l'}, {0xFE, 'm'}, {0xFD, 'n'}, {0xFC, 'o'}, {0xE3, 'p'}, {0xE2, 'q'}, {0xE1, 'r'}, {0xE0, 's'}, {0xE7, 't'}, {0xE6, 'u'}, {0xE5, 'v'}, {0xE4, 'w'}, {0xEB, 'x'}, {0xEA, 'y'}, {0xE9, 'Z'},
            {0xAC, '?'}, {0xB3, ' '}, {0xB8, '+'}, {0xBE, '-'}, {0xB9, '*'}, {0xBC, '/'}, {0xAE, '='}, {0xCF, '\\'}, {0xCD, '^'}, {0xB6, '%'}, {0xE8, '{'}, {0xEE, '}'}, {0xC8, '['}, {0xCE, ']'}, {0xBB, '('}, {0xBA, ')'}, {0xCC, '_'},
            {0xA3, '0'}, {0xA2, '1'}, {0xA1, '2'}, {0xA0, '3'}, {0xA7, '4'}, {0xA6, '5'}, {0xA5, '6'}, {0xA4, '7'}, {0xAB, '8'}, {0xAA, '9'}, {0xB5, '\n'}, {0xBD, '.'}, {0xBF,','}, {0xA9,':'}, {0xAF,'<'},{0xAD,'>'}
        };
        
        static char WeirdByteToChar(int what, bool verbose = false) {
            if (WeirdCodingTable.ContainsKey(what))
            {
                return WeirdCodingTable[what];
            }
            else
            {
                if(verbose)Console.WriteLine("WARN: abnormal symbol " + what);
                return '~';
            }
        }

        static string WeirdStringDecode(string initial)
        {
            StringBuilder sb = new StringBuilder();
            foreach (char i in initial)
            {
                sb.Append(WeirdByteToChar((int)i));
            }
            return sb.ToString();
        }
    }
}
