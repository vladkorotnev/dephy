﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace PhyDecoder
{
    class Program
    {
        static void usage()
        {
            Console.WriteLine("Usage: dephy.exe [-o=path/to/outfile.txt] [-na] [-h] [-v] <path/to/resource.001>[?section,section...] [path/to/resource.002[:section,section...]]");
            Console.WriteLine("-o=...              : output to a txt file");
            Console.WriteLine("-na                 : do not mark correct answers");
            Console.WriteLine("-h                  : print only the file header");
            Console.WriteLine("-v                  : verbose decoding log");
            Console.WriteLine("?section,section... : output only specified sections of file (0-based)");
        }
        static StreamWriter outfile;
        static void outp(string what)
        {
            if (outfile != null)
            {
                outfile.WriteLine(what.Replace("\n","\r\n"));
                outfile.Flush();
            }
            else
            {
                Console.WriteLine(what);
            }
        }
        static string separator = "-------------------------";
        static void Main(string[] args)
        {
            Console.WriteLine("DePhy v1.0a0002 USATU Physics Test Unpacker" + "  by Genjitsu Gadget Lab" );
            if (args.Count() == 0)
            {
                usage(); return;
            }
            List<string> filesToProcess = new List<string>();
            bool onlyHeaders = false;
            bool markAns = true;
            bool verbose = false;
            foreach (string a in args)
            {
                if (a.StartsWith("-"))
                {
                    if (a.StartsWith("-o="))
                    {
                        string fp = a.Substring("-o=".Length);
                        outfile = new StreamWriter(File.OpenWrite(fp));
                    }
                    else if (a.Equals("-na"))
                    {
                        markAns = false;
                    }
                    else if (a.Equals("-h"))
                    {
                        onlyHeaders = true;
                    }
                    else if (a.Equals("-v"))
                    {
                        verbose = true;
                    }
                }
                else
                {
                    var spl = a.Split('?');
                    string fname = spl[0];
                    if (File.Exists(fname))
                        filesToProcess.Add(a);
                    else
                    {
                        Console.WriteLine(fname + " does not exist");
                        return;
                    }
                }
            }
            if (filesToProcess.Count == 0) { usage(); return; }
            foreach (string filepath in filesToProcess)
            {
                var spl = filepath.Split('?');
                bool allSections = true;
                List<int> onlySections = new List<int>();
                if (spl.Count() > 1 && spl[1].Length > 0)
                {
                    allSections = false;
                    foreach (string snum in spl[1].Split(','))
                    {
                        onlySections.Add(Convert.ToInt32(snum));
                    }
                }
                DecodedQuestionnaire qs = Decoder.Decode(spl[0], verbose);
                outp("TOPIC: " + qs.Title + "["+qs.Sections.Count+"]");
                foreach (DecodedSection s in qs.Sections)
                {

                    if (allSections || onlySections.Contains(qs.Sections.IndexOf(s)))
                    {
                        outp("SECTION[" + qs.Sections.IndexOf(s) + "]: " + s.Title + " [" + s.Questions.Count + "]");
                        if (!onlyHeaders)
                        {
                            foreach (DecodedQuestion q in s.Questions)
                            {
                                outp("  " + q.Text);
                                for (int i = 0; i < q.Answers.Count; i++)
                                {
                                    outp("      " + ((q.CorrectAnswer == i && markAns) ? "+" : "-") + " " + q.Answers[i]);
                                }
                                outp(separator);
                            }
                        }
                    }
                }
            }
        }
    }
}
